import {formatDate} from '@/utils/functions';
import {Card, Avatar } from 'antd'

const { Meta } = Card;

const PortfolioCard = ({portfolio}) => {
    return (
            <Card
                hoverable
                style={{width: "95%"}}
                cover={<img alt="example" src={"/img/company/"+portfolio.img}/>}
            >
                <Meta
                    title={<>
                        <p style={{fontWeight: 'bold'}}>{portfolio.title}</p>
                        <p>{portfolio.jobTitle}</p>
                        <p style={{color: 'green'}}>{formatDate(portfolio.startDate)+ "-" + (portfolio.endDate && formatDate(portfolio.endDate) || 'Present')}</p>
                    </>}
                    description={portfolio.companyWebsite}
                />
            </Card>
    )
}

export default PortfolioCard;
