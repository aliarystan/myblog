const Hero = () =>
    <section className="fj-hero">
        <div className="fj-hero-wrapper row">
            <div className="hero-left col-md-6">
                <h1 className="white hero-title">Serik Aliarystan</h1>
                <h1 className="white hero-subtitle">FULL STACK DEVELOPER</h1>
                <h1 className="red hero-subtitle" style={{color:'red'}}>worst designer:)</h1>
            </div>
            <div className="hero-right col-md-6">
                <div className="hero-image-container">
                    <img
                        className="hero-image"
                        src="/img/hero-img.jpg"></img>
                </div>
            </div>
        </div>
    </section>

export default Hero;
