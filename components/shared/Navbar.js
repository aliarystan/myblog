import {useState, useEffect} from 'react';
import {Navbar, Nav, NavDropdown} from 'react-bootstrap';
import Link from 'next/link'
import withApollo from '@/hoc/withApollo';
import {useLazyGetUser} from '@/apollo/actions';
import {Menu, Dropdown, Button, message, Tooltip} from 'antd';
import {DownOutlined, UserOutlined} from '@ant-design/icons';

const AppLink = ({children, className, href, as}) =>
    <Link href={href} as={as}>
        <a className={className}>{children}</a>
    </Link>

const AppNavbar = () => {
    const [user, setUser] = useState(null);
    const [hasResponse, setHasResponse] = useState(false);
    const [getUser, {data, error}] = useLazyGetUser();

    useEffect(() => {
        getUser();
    }, []);

    if (data) {
        if (data.user && !user) {
            setUser(data.user);
        }
        if (!data.user && user) {
            setUser(null);
        }
        if (!hasResponse) {
            setHasResponse(true);
        }
    }
    const menu = (
        <Menu>
            <Menu.Item key="1">
                <AppLink href="/about/who" className="nav-link mr-3">
                    Who am I?
                </AppLink>
            </Menu.Item>
            <Menu.Item key="2">
                <AppLink href="/about/what" className="nav-link mr-3">
                    What am I?
                </AppLink>
            </Menu.Item>
            <Menu.Item key="3">
                <AppLink href="/about/why" className="nav-link mr-3">
                    Why am I?
                </AppLink>
            </Menu.Item>
            <Menu.Item key="3">
                <AppLink href="/about/when" className="nav-link mr-3">
                    When am I?
                </AppLink>
            </Menu.Item>
            <Menu.Item key="3">
                <AppLink href="/about/where" className="nav-link mr-3">
                    Where am I?
                </AppLink>
            </Menu.Item>
            <Menu.Item key="3">
                <AppLink href="/about/how" className="nav-link mr-3">
                    How am I?
                </AppLink>
            </Menu.Item>
            <Menu.Item key="3">
                <AppLink href="/about/can" className="nav-link mr-3">
                    Can I?
                </AppLink>
            </Menu.Item>
        </Menu>
    );

    return (
        <div className="navbar-wrapper">
            <Navbar expand="lg" className="navbar-dark fj-mw9">
                <AppLink
                    href="/"
                    className="navbar-brand mr-3 font-weight-bold">
                    serik.aliarystan
                </AppLink>
                <Navbar.Toggle/>
                <Navbar.Collapse>
                    <Nav className="mr-auto">
                        <Dropdown overlay={menu}>
                            <a className="ant-dropdown-link nav-link mr-3" style={{color: "rgba(255,255,255,.5)"}}
                               onClick={e => e.preventDefault()}>
                                About me
                            </a>
                        </Dropdown>
                        <AppLink href="/portfolios" className="nav-link mr-3">
                            Career
                        </AppLink>
                        <AppLink href="/skills" className="nav-link mr-3">
                            Skills
                        </AppLink>
                        <AppLink href="/forum/categories" className="nav-link mr-3">
                            Forum
                        </AppLink>
                        <AppLink href="/cv" className="mr-3 nav-link">
                            CV
                        </AppLink>
                    </Nav>
                    {hasResponse &&
                    <Nav>
                        {user &&
                        <>
                            <span className="nav-link mr-2">Welcome {user.username}</span>

                            <NavDropdown className="mr-2" title="Manage" id="basic-nav-dropdown">
                                {(user.role === 'admin' || user.role === 'instructor') &&
                                <>
                                    <AppLink href="/portfolios/new" className="dropdown-item">
                                        Create Portfolio
                                    </AppLink>
                                    <AppLink
                                        href="/instructor/[id]/dashboard"
                                        as={`/instructor/${user._id}/dashboard`}
                                        className="dropdown-item">
                                        Dashboard
                                    </AppLink>
                                </>
                                }
                            </NavDropdown>
                            <AppLink href="/logout" className="nav-link btn btn-danger">
                                Sign Out
                            </AppLink>
                        </>
                        }
                        {(error || !user) &&
                        <>
                            <AppLink href="/login" className="mr-3 nav-link">
                                Sign In
                            </AppLink>
                            <AppLink href="/register" className="mr-3 btn btn-success bg-green-2 bright">
                                Sign Up
                            </AppLink>
                        </>
                        }
                    </Nav>
                    }
                </Navbar.Collapse>
            </Navbar>
        </div>
    )
}

export default withApollo(AppNavbar);
