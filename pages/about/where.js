import BaseLayout from '@/layouts/BaseLayout';

const Can = () => {
    return (
        <BaseLayout>
            <div className="bwm-form mt-5">
                <div className="row">
                    <div className="col-md-5 mx-auto">
                        <h1 className="page-title">WHERE AM I</h1>
                        Soon, There will be articles about the places where I have been
                    </div>
                </div>
            </div>
        </BaseLayout>
    )
}


export default Can;
