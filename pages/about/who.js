import BaseLayout from '@/layouts/BaseLayout';

const Can = () => {
    return (
        <BaseLayout>
            <div className="bwm-form mt-5">
                <div className="row">
                    <div className="col-md-5 mx-auto">
                        <h1 className="page-title">WHO AM I?</h1>

                        Hello! I’m Serik Aliarystan.
                        Web Developer from Astana, Kazakhstan.
                        I have rich experience in web development. I love to talk with you about our unique experience .
                    </div>
                </div>
            </div>
        </BaseLayout>
    )
}


export default Can;
