import BaseLayout from '@/layouts/BaseLayout';

const Can = () => {
    return (
        <BaseLayout>
            <div className="bwm-form mt-5">
                <div className="row">
                    <div className="col-md-5 mx-auto">
                        <h1 className="page-title">CAN I?</h1>

                        Yes, I can:)
                    </div>
                </div>
            </div>
        </BaseLayout>
    )
}


export default Can;
