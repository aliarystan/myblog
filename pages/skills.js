import BaseLayout from '@/layouts/BaseLayout';
import {Row, Col, Divider, Progress} from "antd";
import {ProgressBar} from 'react-bootstrap';

const Skills = () => {
    return (
        <BaseLayout>


          {/*  <h1 style={{fontWeight: "bold", textAlign: 'center'}}>My Skills</h1>

            <Divider style={{fontSize: "30px"}}>Base Web</Divider>
            <Row>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250}  type="circle" percent={80}
                              format={() => <span style={{fontSize: '18px'}}>HTML 80%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={80}
                              format={() => <span style={{fontSize: '18px'}}>CSS 80%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250}  type="circle" percent={50}
                              format={() => <span style={{fontSize: '18px'}}>JS 80%</span>}/>
                </Col>
            </Row>*/}
            <Divider style={{fontSize: "30px"}}>React</Divider>
            <Row>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={80} format={() =>
                        <span style={{fontSize: '18px'}}>React 80%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={70} format={() =>
                        <span style={{fontSize: '18px'}}>Next.js 70%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250}  type="circle" percent={50} format={() =>
                        <span style={{fontSize: '18px'}}>React Native 50%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={20} format={() =>
                        <span style={{fontSize: '18px'}}>React VR 20%</span>}/>
                </Col>
            </Row>
            <Divider style={{fontSize: "30px"}}>Java</Divider>
            <Row>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress style={{align:"center"}} width={250} type="circle" percent={90}
                              format={() => <span style={{fontSize: '18px'}}>Java Core 90%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={80}
                              format={() => <span style={{fontSize: '18px'}}>Spring 80%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250}  type="circle" percent={60}
                              format={() => <span style={{fontSize: '18px'}}>Java EE 60%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={20}
                              format={() => <span style={{fontSize: '18px'}}>Play 20%</span>}/>
                </Col>
            </Row>
            <Divider style={{fontSize: "30px"}}>Database</Divider>
            <Row>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={40}
                              format={() => <span style={{fontSize: '18px'}}>Postgres 60%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={40}
                              format={() => <span style={{fontSize: '18px'}}>MongoDB 40%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={40}
                              format={() => <span style={{fontSize: '18px'}}>MySQL 50%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={40}
                              format={() => <span style={{fontSize: '18px'}}>Oracle 30%</span>}/>
                </Col>
            </Row>
            <Divider style={{fontSize: "30px"}}>Other</Divider>
            <Row>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={90}
                              format={() => <span style={{fontSize: '18px'}}>Svelte 90%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={80}
                              format={() => <span style={{fontSize: '18px'}}>Express.js 80%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250}  type="circle" percent={60}
                              format={() => <span style={{fontSize: '18px'}}>Next.js 60%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={50}
                              format={() => <span style={{fontSize: '18px'}}>Django 50%</span>}/>
                </Col>
            </Row>
            <Divider style={{fontSize: "30px"}}>Features</Divider>
            <Row>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250} type="circle" percent={40}
                              format={() => <span style={{fontSize: '18px'}}>Docker 40%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250}  type="circle" percent={50}
                              format={() => <span style={{fontSize: '18px'}}>Git 50%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250}  type="circle" percent={50}
                              format={() => <span style={{fontSize: '18px'}}>WebPack 50%</span>}/>
                </Col>
                <Col xl={6} sm={12} xs={24} className={"center"}>
                    <Progress width={250}  type="circle" percent={50}
                              format={() => <span style={{fontSize: '18px'}}>Nignx 50%</span>}/>
                </Col>
            </Row>


        </BaseLayout>
    )
}


export default Skills;
